﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.Util;

namespace MockSapSync
{
    [DataContract]
    public class AppworksTask
    {

        [DataMember(Name = "external_id")]
        public string ExternalId { get; set; }

        [DataMember(Name = "previous_error")]
        public ImportError PreviousError { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "importance")]
        public string Importance { get; set; }

        [DataMember(Name = "customer_record_external_id")]
        public string CustomerId { get; set; }

        [DataMember(Name = "project_external_id")]
        public string ProjectId { get; set; }

        [DataMember(Name = "assignee_external_id")]
        public string AssigneeId { get; set; }

        [DataMember(Name = "place_of_work_address")]
        public PostalAddress PlaceOfConsumptionAddress { get; set; }

        [DataMember(Name = "finalization_time")]
        public string FinalizationTime { get; set; }

        [DataMember(Name = "estimated_time")]
        public int EstimatedTime { get; set; }

        [DataMember(Name = "agreed_time")]
        public string AgreedTime { get; set; }

        [DataMember(Name = "deadline")]
        public string Deadline { get; set; }

        [DataMember(Name = "form")]
        public FormData FormData { get; set; }

        public string GetStringFormValue(string fieldId)
        {
            return FormData.FormFields.Where(field=>field.FieldDataType.Equals(FormFieldDataTypeSelector.STRING.ToString()))
                .FirstOrDefault(field => field.FieldId.Equals(fieldId))?.StringValue;
        }

        public bool? GetBoolFormValue(string fieldId)
        {
            return FormData.FormFields.Where(field => field.FieldDataType.Equals(FormFieldDataTypeSelector.BOOLEAN.ToString()))
                .FirstOrDefault(field => field.FieldId.Equals(fieldId))?.BooleanValue;
        }

        public decimal? GetDecimalFormValue(string fieldId)
        {
            return FormData.FormFields.Where(field => field.FieldDataType.Equals(FormFieldDataTypeSelector.DECIMAL.ToString()))
                .FirstOrDefault(field => field.FieldId.Equals(fieldId))?.DecimalValue;
        }

        public long? GetNumberFormValue(string fieldId)
        {
            return FormData.FormFields.Where(field => field.FieldDataType.Equals(FormFieldDataTypeSelector.NUMBER.ToString()))
                .FirstOrDefault(field => field.FieldId.Equals(fieldId))?.NumberValue;
        }

        public List<string> GetListFormValue(string fieldId, string listTypeId)
        {
            return FormData.FormFields.Where(field => field.FieldDataType.Equals(FormFieldDataTypeSelector.LIST_ITEM.ToString()) && field.ListItemTypeId.Equals(listTypeId))
                .FirstOrDefault(field => field.FieldId.Equals(fieldId))?.ListItems?.Select(item => item.Code).ToList();
        }

        public List<string> GetMultiselectListFormValue(string fieldId, string listTypeId)
        {
            return FormData.FormFields.Where(field => field.FieldDataType.Equals(FormFieldDataTypeSelector.LIST_MULTI_ITEM.ToString()) && field.ListItemTypeId.Equals(listTypeId))
                .FirstOrDefault(field => field.FieldId.Equals(fieldId))?.ListItems?.Select(item => item.Code).ToList();
        }

        public string GetPhoneNumberFormValue(string fieldId)
        {
            return FormData.FormFields.Where(field => field.FieldDataType.Equals(FormFieldDataTypeSelector.PHONE_NUMBER.ToString()))
                .FirstOrDefault(field => field.FieldId.Equals(fieldId))?.PhoneNumberValue;
        }

        public string GetEmailAddressFormValue(string fieldId)
        {
            return FormData.FormFields.Where(field => field.FieldDataType.Equals(FormFieldDataTypeSelector.EMAIL_ADDRESS.ToString()))
                .FirstOrDefault(field => field.FieldId.Equals(fieldId))?.EmailAddressValue;
        }

        public DateTime? GetDateFormValue(string fieldId)
        {
            var date = FormData.FormFields.Where(field => field.FieldDataType.Equals(FormFieldDataTypeSelector.DATE.ToString()))
                .FirstOrDefault(field => field.FieldId.Equals(fieldId))?.DateValue;
            if (date == null) return null;
            return DateTime.Parse(date);
        }

        public DateTime? GetDateTimeFormValue(string fieldId)
        {
            var date = FormData.FormFields.Where(field => field.FieldDataType.Equals(FormFieldDataTypeSelector.DATE_TIME.ToString()))
                .FirstOrDefault(field => field.FieldId.Equals(fieldId))?.DateTimeValue;
            if (date == null) return null;
            return DateTime.Parse(date);
        }
    }

    [DataContract]
    public class FormData
    {
        [DataMember(Name = "fields")]
        public List<FormField> FormFields { get; set; }
    }

    [DataContract]
    public class ImportError
    {
        [DataMember(Name = "error_code")]
        public string ErrorCode { get; set; }

        [DataMember(Name = "error_message")]
        public string Message { get; set; }
    }

    [DataContract]
    public class FormField
    {
        [DataMember(Name = "external_id")]
        public string FieldId { get; set; }

        [DataMember(Name = "field_data_type")]
        public string FieldDataType { get; set; }

        [DataMember(Name = "string_value")]
        public string StringValue { get; set; }

        [DataMember(Name = "decimal_value")]
        public decimal? DecimalValue { get; set; }

        [DataMember(Name = "number_value")]
        public long? NumberValue { get; set; }

        [DataMember(Name = "date_value")]
        public string DateValue { get; set; }

        [DataMember(Name = "date_time_value")]
        public string DateTimeValue { get; set; }

        [DataMember(Name = "boolean_value")]
        public bool? BooleanValue { get; set; }

        [DataMember(Name = "email_address_value")]
        public string EmailAddressValue { get; set; }

        [DataMember(Name = "phone_number_value")]
        public string PhoneNumberValue { get; set; }

        [DataMember(Name = "list_item_type_external_id")]
        public string ListItemTypeId { get; set; }

        [DataMember(Name = "list_items")]
        public List<ListItem> ListItems { get; set; }

        [DataMember(Name = "master_data_record_external_ids")]
        public List<string> MasterDataRecordIds { get; set; }

        [DataMember(Name = "stock_data")]
        public StockData StockData { get; set; }

    }

    [DataContract]
    public class ListItem
    {
        [DataMember(Name = "code")]
        public string Code { get; set; }
    }

    [DataContract]
    public class StockData
    {
        [DataMember(Name = "discount_percent")]
        public decimal? DiscountPercent { get; set; }

        [DataMember(Name = "items")]
        public List<StockItem> items { get; set; }

        [DataMember(Name = "coordinate")]
        public Coordinate coordinate { get; set; }
    }

    [DataContract]
    public class StockItem
    {
        [DataMember(Name = "external_id")]
        public string ExternalId { get; set; }

        [DataMember(Name = "stock_item_type")]
        public string StockItemType { get; set; }

        [DataMember(Name = "amount")]
        public decimal? Amount { get; set; }

        [DataMember(Name = "custom_net_price")]
        public decimal? CustomNetPrice { get; set; }
    }

    [DataContract]
    public class PostalAddress
    {
        [DataMember(Name = "complex_address")]
        public Address ComplexAddress { get; set; }

        [DataMember(Name = "text_address")]
        public string textAddress { get; set; }

        [DataMember(Name = "coordinate")]
        public Coordinate coordinate { get; set; }
    }

    [DataContract]
    public class Address
    {

        [DataMember(Name = "country_code")]
        public string CountryCode { get; set; }

        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "zip_code")]
        public string ZipCode { get; set; }

        [DataMember(Name = "address")]
        public string AddressText { get; set; }

        [DataMember(Name = "address_type")]
        public string AddressType { get; set; }

        [DataMember(Name = "house_number")]
        public string HouseNumber { get; set; }

        [DataMember(Name = "building")]
        public string Building { get; set; }

        [DataMember(Name = "stairway")]
        public string Stairway { get; set; }

        [DataMember(Name = "floor")]
        public string Floor { get; set; }

        [DataMember(Name = "door")]
        public string Door { get; set; }

        public override string ToString()
        {
            return City + " " + ZipCode + " " + AddressText + " " + AddressType + " " + HouseNumber;
        }
    }

    [DataContract]
    public class Coordinate
    {
        [DataMember(Name = "latitude")]
        public double Lat { get; set; }

        [DataMember(Name = "longitude")]
        public double Lon { get; set; }
    }

    public enum FormFieldDataTypeSelector
    {
        STRING,
        NUMBER,
        DECIMAL,
        DATE,
        DATE_TIME,
        BOOLEAN,
        LIST_ITEM,
        LIST_MULTI_ITEM,
        EMAIL_ADDRESS,
        PHONE_NUMBER,
        MASTER_DATA,
        STOCK,
        READONLY_TEXT,
        READONLY_HTML,
        SURVEY_ITEM
    }


}
