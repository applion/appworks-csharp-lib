﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace MockSapSync
{
    public class FormFieldSampleValues
    {
        public bool CustomerHasNewPhoneNumber { get; set; } 
        public string CustomerNewPhoneNumber { get; set; } 
        public string CustomerNewEmailAddress { get; set; }
        public List<string> CauseOfMountingCodes { get; set; } 
        public string PlaceOfConsuptionComment { get; set; } 
        public string DismountedWaterMeterClassification { get; set; } 
        public decimal? DismountedWaterMeterActualReading { get; set; } 
        public long? MountedWaterMeterAuthYear { get; set; }
        public DateTime? MountedWaterMeterValidFrom { get; set; }
        public string SomeExtraBusinessLogic { get; set; }
        public string FinalizationDateString { get; set; }
        public DateTimeOffset? FinalizationTime { get; set; }
        public List<string> DismountedWaterMeterType { get; set; }

        public override string ToString()
        {
            return $"{nameof(CustomerHasNewPhoneNumber)}: {CustomerHasNewPhoneNumber}, {nameof(CustomerNewPhoneNumber)}: {CustomerNewPhoneNumber}, {nameof(CustomerNewEmailAddress)}: {CustomerNewEmailAddress}, {nameof(CauseOfMountingCodes)}: {CauseOfMountingCodes}, {nameof(PlaceOfConsuptionComment)}: {PlaceOfConsuptionComment}, {nameof(DismountedWaterMeterClassification)}: {DismountedWaterMeterClassification}, {nameof(DismountedWaterMeterActualReading)}: {DismountedWaterMeterActualReading}, {nameof(MountedWaterMeterAuthYear)}: {MountedWaterMeterAuthYear}, {nameof(MountedWaterMeterValidFrom)}: {MountedWaterMeterValidFrom}, {nameof(SomeExtraBusinessLogic)}: {SomeExtraBusinessLogic}, {nameof(FinalizationDateString)}: {FinalizationDateString}, {nameof(FinalizationTime)}: {FinalizationTime}";
        }
    }
}