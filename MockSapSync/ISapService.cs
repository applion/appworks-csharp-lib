﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace MockSapSync
{
    [ServiceContract]
    public interface ISapService
    {
        [OperationContract(Name = "upload")]
        SapResponse Upload(AppworksTask request);
    }

    [DataContract]
    public class SapResponse
    {
        [DataMember(Name = "error_message")]
        public string ErrorMessage { get; set; }
        
        [DataMember(Name = "error_code")]
        public string ErrorCode { get; set; }     
    }
}
