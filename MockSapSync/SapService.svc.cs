﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceModel.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace MockSapSync
{
  public class SapService : ISapService
    {
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        public SapResponse Upload(AppworksTask request)
        {
            Debug.WriteLine(request.ToString());
            Debug.WriteLine(JsonConvert.SerializeObject(request, new IsoDateTimeConverter()));

            //Parsing dateTime value
            DateTimeOffset? someDatetime = request.FinalizationTime != null
                ? (DateTimeOffset?) DateTimeOffset.Parse(request.FinalizationTime)
                : null;
            string someStringDate = null;
            if (someDatetime.HasValue)
            {
                var dateTime = someDatetime.Value;
                someStringDate = dateTime.ToString("yyyyMMdd");
            }

            //form fields example
            var exampleFields = new FormFieldSampleValues
            {
                //bool field 
                CustomerHasNewPhoneNumber = request.GetBoolFormValue("customer_new_phonenumber_cb") ?? false,
                //phoneNumber field
                CustomerNewPhoneNumber = request.GetPhoneNumberFormValue("customer_new_phonenumber"),
                //emailAddress field
                CustomerNewEmailAddress = request.GetEmailAddressFormValue("customer_new_email"),
                //string field
                PlaceOfConsuptionComment = request.GetStringFormValue("place_of_consumption_comment"),
                //number field
                MountedWaterMeterAuthYear = request.GetNumberFormValue("mounted_water_meter_auth_year"),
                //deciaml value
                DismountedWaterMeterActualReading = request.GetDecimalFormValue("dismounted_water_meter_actual_reading"),
                //date value
                MountedWaterMeterValidFrom = request.GetDateFormValue("mounted_water_meter_valid_from"),
                //list value
                CauseOfMountingCodes = request.GetListFormValue("cause_of_mounting", "cause_of_mounting"),
                //multi list value
                DismountedWaterMeterType = request.GetMultiselectListFormValue("dismounted_water_meter_type", "water_meter_type"),
                FinalizationTime = someDatetime,
                FinalizationDateString =  someStringDate
            };
            //If you need extra business logic:
            var formattedDate = exampleFields.MountedWaterMeterValidFrom?.ToString("yyyyMMdd") ?? "";
            exampleFields.SomeExtraBusinessLogic = $"{exampleFields.PlaceOfConsuptionComment}_{exampleFields.MountedWaterMeterAuthYear}_{formattedDate}";

            //previous errorCode
            string prevErrorCode = request.PreviousError?.ErrorCode; 

            //place of work address
            string placeOfWokAddress = request.PlaceOfConsumptionAddress.textAddress ??
                             request.PlaceOfConsumptionAddress.ComplexAddress.ToString();
            //multi list form
            List<string> multiSelectList = request.GetMultiselectListFormValue("extId", "listType");

            //WebOperationContext ctx = WebOperationContext.Current;
            //ctx.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;

            return new SapResponse
            {
                //some example result
                ErrorMessage = $"{request} {exampleFields}",
                //error code string
                ErrorCode = "ABC100"
            };
        }

    }
}
